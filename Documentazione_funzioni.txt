SOLUZIONE PROPOSTA

Analisi
La richiesta posta, prevede lo sviluppo di un algoritmo che segna il calcolo dei minuti di lavoro
svolti da un dipendente di un’azienda. I dati utili al calcolo del tempo lavorativo (orario di
entrata, orario di uscita e codice di riconoscimento del dipendete), saranno prelevati da un file
di testo.
Inserendo da linea di comando uno dei codici dipendente, il programma dovrà mostrare a video
il totale dei minuti che quel dipendente ha lavorato. Qualora quel dipendente assente, il
programma dovrà visualizzare la lista completa dei dipendenti che quel giorno erano presenti in
azienda.



Descrizione Funzioni:

int leggi(char* nomefile, struct dipendenti* accessi)
Scopo:
leggere i passaggi dal file e memorizzarli nella struct accessi

Descrizione parametri:
char* nomefile: puntatore all&#39;array di caratteri contenente il nome del file da aprire.
struct dipendenti* accessi: puntatore alla struttura che conterra&#39; i passaggi letti da file.

Descrizione output prodotto:
return 1: OK
return 0: ERRORE




int opzione1(struct dipendenti accessi)

Scopo:
restituire il numero totale di dipendenti DIVERSI entrati nell&#39;azienda

Descrizione Parametri:
struct dipendenti accessi: copia della struttura contenente i passaggi letti da file

Descrizione output prodotto:
return cont (numero dipendenti diversi trovati)




void opzione2(char matricola[], struct dipendenti accessi)

Scopo:
restituire il numero totale di minuti in cui il dipendente scelto e&#39; stato all&#39;interno dell&#39;azienda

Descrizione parametri:
char matricola[]: nome della matricola (passata come parametro al main)
struct dipendenti accesso: copia della struttura contenente i passaggi letti da file.

Descrizione output:
OK: printf "il dipendente abc123 ha lavorato 200 minuti";
ERR: printf "il dipendente ha effettuato solo un passaggio";
ERR: printf "dipendente non trovato";



Descrizione Main:

int argc = contatore che ci dice il numero di parametri passati al main.

char** argv = matrice di caratteri:
 argv[1][] = nome del file da aprire
 argv[2][] = nome della matrice

if(arcg == 2) -> se e' stato passato un solo parametro (oltre al nome del programma)
if(argc == 3) -> se sono stati passati due parametri (oltre al nome del programma)

il ciclo for serve semplicemente a trasformare la matrice in minuscolo, per evitare errori
banali.